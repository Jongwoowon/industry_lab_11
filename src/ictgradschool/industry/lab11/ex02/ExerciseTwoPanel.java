package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JTextField firstField;

    private JTextField secondField;

    private JTextField resultField;

    private JButton addButton;

    private JButton subtractButton;


    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        firstField = new JTextField(10);
        this.add(firstField);

        secondField = new JTextField(10);
        this.add(secondField);

        resultField = new JTextField(20);


        addButton = new JButton("Add");
        this.add(addButton);

        subtractButton = new JButton("Subtract");
        this.add(subtractButton);

        JLabel resultLabel = new JLabel("Result: ");
        this.add(resultLabel);

        this.add(resultField);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * *@param amount to round as a double
     * @return the amount rounded to 2dp
     */

    public void actionPerformed(ActionEvent event) {

        double firstNo = roundTo2DecimalPlaces(Double.parseDouble(firstField.getText()));
        double secondNo = roundTo2DecimalPlaces(Double.parseDouble(secondField.getText()));

        if (event.getSource() == addButton) {
            String resultNo = Double.toString(firstNo + secondNo);
            resultField.setText(resultNo);
        }

        else if (event.getSource() == subtractButton) {
            String resultNo = Double.toString(firstNo - secondNo);
            resultField.setText(resultNo);
        }

    }

        private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}