package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.

    private JTextField heightText;

    private JTextField weightText;

    private JTextField bmiText;

    private JTextField hWeight;

    private JButton calculateBMIButton;

    private JButton calculateHWButton;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        heightText = new JTextField(14);

        weightText = new JTextField(14);

        bmiText = new JTextField(14);

        hWeight = new JTextField(14);

        calculateBMIButton = new JButton("Calculate BMI");

        calculateHWButton = new JButton ("Calculate Healthy Weight");

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightLabel = new JLabel("Height in metres:");

        JLabel weightLabel = new JLabel("Weight in kilograms:");

        JLabel bmiLabel = new JLabel("Your Body Mass Index (BMI) is:");

        JLabel hLabel = new JLabel("Maximum Healthy Weight for your Height:");


        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        this.add(heightLabel);
        this.add(heightText);

        this.add(weightLabel);
        this.add(weightText);

        this.add(calculateBMIButton);
        this.add(bmiLabel);
        this.add(bmiText);

        this.add(calculateHWButton);
        this.add(hLabel);
        this.add(hWeight);

        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHWButton.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be displayed in the text box.

        double thisHeight = Double.parseDouble(heightText.getText());

        //A. The following calculates BMI and sets this to bmiText

        if (event.getSource() == calculateBMIButton){

            double thisWeight = Double.parseDouble(weightText.getText());
            String theBmi = Double.toString(roundTo2DecimalPlaces(thisWeight/ (thisHeight * thisHeight)));
             bmiText.setText(theBmi) ;
        }

        else if (event.getSource() == calculateHWButton){
            String healthyWeight = Double.toString(roundTo2DecimalPlaces(24.9 * thisHeight * thisHeight));
            hWeight.setText(healthyWeight);
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}