package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener, MouseListener {

    private  Balloon balloon;
    private  JButton moveButton;
    private Timer timer;
    private ArrayList<Balloon> balloonArray;
//        balloonArray;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);



        this.balloonArray = new ArrayList<Balloon>();

        balloonArray.add(new Balloon(30, 60));

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);

        this.addKeyListener(this);
        this.addMouseListener(this);

        this.timer = new Timer(200, this);
//
//
//
//
//        int i;
//        while ()
//        }
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (int i = 0; i < balloonArray.size(); i++){
            if (e.getSource() == timer) {
                balloonArray.get(i).move();
            }
        }


        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for(Balloon balloon : this.balloonArray) {
            balloon.draw(g);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {


        for (int i = 0; i < balloonArray.size(); i++){
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                balloonArray.get(i).setDirection(Direction.Left);
                timer.start();
            }

            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                balloonArray.get(i).setDirection(Direction.Right);
                timer.start();
            }

            else if (e.getKeyCode() == KeyEvent.VK_UP) {
                balloonArray.get(i).setDirection(Direction.Up);
                timer.start();
            }

            else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                balloonArray.get(i).setDirection(Direction.Down);
                timer.start();
            }


            else if (e.getKeyCode() == KeyEvent.VK_S) {
                timer.stop();
            }
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Point center_point = e.getPoint();
        Balloon newBalloon;

        newBalloon = new Balloon(center_point.x, center_point.y);
        balloonArray.add(newBalloon);
        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}